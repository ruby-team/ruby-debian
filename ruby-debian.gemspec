Gem::Specification.new do |spec|
  spec.name          = "ruby-debian"
  spec.version       = '0.3.10'
  spec.authors       = ["Antonio Terceiro"]
  spec.email         = ["terceiro@debian.org"]

  spec.summary       = %q{ruby interface for dpkg}
  spec.homepage      = "https://salsa.debian.org/ruby-team/ruby-debian"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir['**/*'] - Dir['debian/**/*']
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = Dir["t/**/*"]
  spec.require_paths = ["lib"]
  spec.extensions    = ["ext/debian_version/extconf.rb"]
end
